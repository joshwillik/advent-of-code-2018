use std::collections::{HashMap};
const INPUT: &str = include_str!("../resources/day2.txt");

pub fn part_a() -> usize {
    let box_ids = parse_box_ids();
    count_repeat_char_lines(&box_ids, 2) * count_repeat_char_lines(&box_ids, 3)
}

pub fn part_b() -> String {
    let box_ids = parse_box_ids();
    for (i, id) in box_ids.iter().enumerate() {
        for id2 in box_ids[i..].iter() {
            if (string_distance(&id, &id2)) == 1 {
                return string_overlap(&id, &id2);
            }
        }
    }
    panic!("Could not find puzzle solution");
}

// This line has 'static because I don't understand enough about rust lifetimes.
// Maybe 'static is right here because INPUT is a global const?
// Maybe this function should be &str -> Vec<&str>?
fn parse_box_ids() -> Vec<&'static str> {
    INPUT.lines().collect()
}

fn count_repeat_char_lines(box_ids: &Vec<&str>, num: usize) -> usize {
    box_ids.iter().filter(|x| {
        let mut char_map = HashMap::new();
        for c in x.chars() {
            let v = char_map.entry(c).or_insert(0);
            *v += 1;
        };
        char_map.values().any(|c_num| *c_num == num)
    }).count()
}

fn string_distance(a: &str, b: &str) -> usize {
    a.len() - string_overlap(a, b).len()
}

fn string_overlap(a: &str, b: &str) -> String {
    a.chars()
        .zip(b.chars())
        .filter_map(|(a, b)| if a==b { Some(a) } else { None })
        .collect()
}
