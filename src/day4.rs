extern crate regex;
extern crate chrono;
use self::regex::Regex;
use self::chrono::naive::{NaiveDate, NaiveDateTime};
use self::chrono::{Timelike};
use std::cmp::Ordering;
use std::collections::HashMap;

pub fn part_a () -> u32 {
    let sleep_map = build_sleep_map(&parse_input());
    let sleepiest_guard = sleep_map
        .iter()
        .map(|(id, mins)| (id, mins.iter().sum::<u32>()))
        .max_by_key(|(_, mins)| *mins)
        .unwrap()
        .0;
    let sleepiest_minute = sleep_map[sleepiest_guard]
        .iter()
        .enumerate()
        .max_by_key(|(_, i)| *i)
        .unwrap()
        .0 as u32;
    *sleepiest_guard as u32 * sleepiest_minute
}

pub fn part_b () -> u32 {
    let sleep_map = build_sleep_map(&parse_input());
    let (guard, minute) = sleep_map.iter()
        .map(|(guard_id, minutes)| {
            (
                guard_id,
                minutes
                    .iter()
                    .enumerate()
                    .max_by_key(|(_, i)| *i)
                    .unwrap(),
            )
        })
        .max_by_key(|(_, (_, count))| *count)
        .map(|(guard, (minute, _))| (guard, minute))
        .unwrap();
    *guard as u32 * minute as u32
}

fn build_sleep_map(logs: &Vec<Log>) -> HashMap<usize, [u32; 60]> {
    let mut sleep_map = HashMap::new();
    let mut current_guard = None;
    let mut fell_asleep = None;
    for log in logs {
        match log.event {
            Event::BeginShift(guard) => current_guard = Some(guard),
            Event::FallAsleep => fell_asleep = Some(log.time),
            Event::WakeUp => {
                let fell_asleep = fell_asleep.unwrap();
                let guard_entry = sleep_map
                    .entry(current_guard.unwrap())
                    .or_insert([0u32; 60]);
                for m in &mut guard_entry[fell_asleep.minute() as usize..log.time.minute() as usize] {
                    *m += 1;
                }
            },
        };
    }
    sleep_map
}

fn parse_input() -> Vec<Log> {
    let mut logs: Vec<Log> = include_str!("../resources/day4.txt")
        .lines()
        .map(parse_log)
        // TODO: find out why I can't use sorted_by_key here
        // .sorted_by_key(|e| e.time);
        .collect();
    logs.sort();
    logs
}

fn parse_log(desc: &str) -> Log {
    lazy_static! {
        static ref LOG_RE: Regex = Regex::new(r"(?x) # flexible whitespace
            \[
                (?P<year>\d{4})
                -
                (?P<month>\d{2})
                -
                (?P<day>\d{2})
                \s
                (?P<hour>\d{2})
                :
                (?P<minute>\d{2})
            \]
            \s
            (?P<event>.*)
        ").unwrap();
        static ref DIGIT_RE: Regex = Regex::new(r"#(\d+)").unwrap();
    }
    let m = LOG_RE.captures(desc).unwrap();
    let field = |name| m.name(name).unwrap().as_str();
    let int = |name| field(name).parse::<u32>().unwrap();
    let time = NaiveDate::from_ymd(
        int("year") as i32,
        int("month"),
        int("day"),
    ).and_hms(
        int("hour"),
        int("minute"),
        0,
    );
    let event = if field("event").starts_with("wakes up") {
        Event::WakeUp
    } else if field("event").starts_with("falls asleep") {
        Event::FallAsleep
    } else {
        let id_str = DIGIT_RE.captures(field("event"))
            .unwrap()
            .get(1)
            .unwrap()
            .as_str();
        let guard_id = id_str.parse().expect(format!("Expected {} to parse as an ID", id_str).as_str());
        Event::BeginShift(guard_id)
    };
    Log {time, event}
}

#[derive(Debug)]
enum Event {
    BeginShift(usize),
    FallAsleep,
    WakeUp,
}

#[derive(Debug)]
struct Log {
    time: NaiveDateTime,
    event: Event,
}

impl Ord for Log {
    fn cmp(&self, other: &Log) -> Ordering {
        self.time.cmp(&other.time)
    }
}

impl PartialOrd for Log {
    fn partial_cmp(&self, other: &Log) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Log {
    fn eq(&self, other: &Log) -> bool {
        self.time == other.time
    }
}

impl Eq for Log {}
