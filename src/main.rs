// Needs to be defined at the site root because it's importing a macro for day4
// I'm not sure why macro_use is not allowed inside a module
#[macro_use]
extern crate lazy_static;

mod day1;
mod day2;
mod day3;
mod day4;

fn main() {
    println!("Day 1:\t{}\t{}", day1::part_a(), day1::part_b());
    println!("Day 2:\t{}\t{}", day2::part_a(), day2::part_b());
    println!("Day 3:\t{}\t{}", day3::part_a(), day3::part_b());
    println!("Day 4:\t{}\t{}", day4::part_a(), day4::part_b());
}
