use std::fs;
use std::collections::HashSet;

pub fn part_a() -> i32 {
    read_input().iter().fold(0, |acc, x| acc + x)
}

pub fn part_b() -> i32 {
    let items = read_input();
    let mut counter = 0;
    let mut sums = HashSet::new();
    for item in items.iter().cycle() {
        counter += item;
        if sums.contains(&counter) {
            break;
        }
        sums.insert(counter);
    };
    counter
}

fn read_input() -> Vec<i32> {
    fs::read_to_string("resources/day1.txt")
        .expect("day1.txt does not exist")
        .lines()
        .map(|x| x.parse::<_>()
            .expect(format!("{} is not a number", x).as_str()))
        .collect()
}
