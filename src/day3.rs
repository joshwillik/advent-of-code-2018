use std::ops::Range;
const INPUT: &str = include_str!("../resources/day3.txt");

pub fn part_a() -> usize {
    overlapping_claims(&parse_input())
}

pub fn part_b() -> usize {
    let claims = parse_input();
    claims.iter().find(|a| {
        claims.iter().filter(|b| b.id != a.id).find(|b| {
            overlaps(a.x_range(), b.x_range()) && overlaps(a.y_range(), b.y_range())
        }).is_none()
    }).unwrap().id
}

fn overlaps(a: Range<usize>, b: Range<usize>) -> bool {
    a.start >= b.start && a.start < b.end ||
        b.start >= a.start && b.start < a.end
}

#[derive(Debug)]
struct Claim {
    id: usize,
    x: usize,
    width: usize,
    y: usize,
    height: usize,
}

impl Claim {
    fn y_range(&self) -> Range<usize> { self.y..self.y+self.height + 1 }
    fn x_range(&self) -> Range<usize> { self.x..self.x+self.width + 1 }
}

fn overlapping_claims(claims: &Vec<Claim>) -> usize {
    let mut grid = new_grid();
    for claim in claims {
        apply_claim(&mut grid, claim)
    }
    grid.iter().fold(0, |acc, row| {
        acc + row.iter().filter(|&&x| x > 1).count()
    })
}

fn new_grid() -> Vec<Vec<usize>> {
    let mut v = Vec::new();
    for _ in 0..1000 {
        let mut row = Vec::new();
        for _ in 0..1000 {
            row.push(0);
        }
        v.push(row);
    }
    v
}

fn apply_claim(grid: &mut Vec<Vec<usize>>, claim: &Claim) {
    for row in grid.iter_mut().skip(claim.y).take(claim.height) {
        for col in row.iter_mut().skip(claim.x).take(claim.width) {
            *col += 1;
        }
    }
}

fn parse_input() -> Vec<Claim> {
    INPUT.lines()
        .map(parse_claim)
        .collect()
}

fn parse_claim(desc: &str) -> Claim {
    let mut parts = desc.split(claim_separator).filter(|c| *c != "");
    let mut eat_token = |name| {
        let next_token = parts.next().expect(&format!("Missing token for {}", name));
        next_token.parse().ok().expect(&format!("Unable to parse {} in {}", name, desc))
    };
    Claim {
        id: eat_token("id"),
        x: eat_token("x"),
        y: eat_token("y"),
        width: eat_token("width"),
        height: eat_token("height"),
    }
}

fn claim_separator(c: char) -> bool {
    ['#', ' ', ',', '@', ':', 'x'].contains(&c)
}
